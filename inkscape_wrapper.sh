#!/bin/bash

if [ ! -f $2 ]; then
 touch $2
 inkscape $1
 rm $2
else
 echo "Already running"
fi
